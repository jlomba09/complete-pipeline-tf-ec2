<div align="center"><h1><ins>DevOps Demo Project:</ins></h1></div>
<div align="center"><h2>Complete CI/CD with Terraform</h2></div>
<div align="center">![1](img/devOps.png)</div>
<br>
<ins>DevOps Tools used in this project</ins>:<br>- Terraform<br>- AWS: EC2 and VPC<br>- Jenkins<br>- Apache: Groovy and Maven<br>- Docker<br>- DockerHub<br>- Git<br>- GitLab<br>- Microsoft Visual Studio Code<br>- IntelliJ IDEA<br>- Java (command for running the JAR)<br>- Windows PowerShell<br>- Windows Command Prompt<br><br>

<ins>Operating systems</ins>:<br>- Linux<br>- Windows 11

<ins>Database</ins>: PostgreSQL

<ins>Project Purpose:</ins> Deploy a complete Jenkins CI/CD pipeline with stages to build a Java Maven artifact, build and push a Docker image to DockerHub, automatically provision an EC2 instance using Terraform, and use Docker Compose to deploy a new Java application version on the provisioned EC2 instance.

This project provides a comprehensive walkthrough of writing a Jenkinsfile and accounts for the necessary prerequisites prior to additional Groovy code being written. A complete Jenkins CI/CD pipeline is successfully executed at the project’s conclusion.

<ins>Out of scope</ins>:<br>- Containerized Jenkins installation<br>- Git installation on Windows 11<br>- GitLab account creation<br>- DockerHub account creation<br>- AWS CLI installation on Windows 11<br>- Writing Java code

# Project Walkthrough
I. [Preliminary Project Tasks](#prelim)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [Clone Java Application Locally](#clone)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [Create Shared Library](#createSharedLib)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C) [Jenkins: Confirm/Add GitLab Credentials](#gitlabCreds)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D) [Update Jenkinsfile](#updateJF1)<br>
II. [CI Stages](#ciStages)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [Stage: Build App](#buildApp)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) [Prerequisite: Maven](#mavenPrereq)<br> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) [Jenkins: Confirm/Configure Build Tool](#mavenPlugin)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) [Update Jenkinsfile](#updateJF2)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii) [Shared Library: Create buildJar.groovy](#buildJar)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iii) [Update Jenkinsfile](#updateJF3)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [Stage: Build Image](#buildImage)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) [Shared Library Prerequisites](#sharedLibPrereq)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) [DockerHub: Confirm Private Repo](#dockerhub)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) [Jenkins:](#jpII)<br> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1) [Credentials Binding Plugin](#credBindings)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2) [Confirm/Install Docker](#installDocker)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3) [DockerHub Credentials](#dockerCreds)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c) [Create Dockerfile](#dockerfile)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii) [Update Shared Library](#updateSharedLib)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) [Create Groovy Class: Docker](#groovyClass)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) [Update vars folder:](#vars)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1) [buildImage.groovy](#buildImageGroovy)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2) [dockerLogin.groovy](#dockerLogin)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3) [dockerPush.groovy](#dockerPush)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
iii) [Update Jenkinsfile](#updateJF4)<br>
III. [CD Stages:](#cdStages)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [Stage: Provision EC2 Server Using Terraform](#provision)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) [Prerequisites:](#prereqIII)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) [AWS: Create SSH Key Pair](#awsSSH)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) [Jenkins:](#jpIII)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1) [Create Pipeline Skeleton](#skeleton)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2) [Credentials for SSH Key Pair](#credsSSH)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3) [Credentials for AWS](#credsAWS)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4) [Install Terraform](#installtf)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c) [Terraform Configuration Files](#tfconfig)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1) [main.tf: VPC Resources & Inputs](#maintfVPC)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2) [main.tf EC2 Resources, Data, & Inputs](#maintfEC2)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3) [variables.tf](#variables)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d) [Bash script: entry-script.sh](#bashEntry)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii) [Update Jenkinsfile](#updateJF5)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [Stage: Deploy new Java app version on EC2 with Docker Compose](#deploy)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) [Prerequisites:](#prereqDeploy)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) [SSH Agent Plugin](#sshagent)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) [Bash Script: Docker Commands](#bashDocker)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c) [Docker Compose](#dockercompose2)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii) [Update Jenkinsfile](#updateJF6)<br>
IV. [Run Pipeline & Troubleshooting](#pipeline)<br>
V. [Project Teardown](#teardown)<br>

    
## I. Preliminary Project Tasks <a name="prelim"></a>
As a precursor to starting the project, a few preliminary project tasks are required. First, the project repository must be cloned locally. Second, a shared library will be created for later use in the 'build app' and 'build image' stages in the pipeline. Then, GitLab credentials will be stored inside Jenkins. Last, the Jenkinsfile will be created.
## A) Clone Java Application Project Locally <a name="clone"></a>
<ins>Note</ins>: Although the installation of Git on Windows 11 is outside the scope of this project walkthrough, the Git installer can be found here: https://git-scm.com/download/win

Assume the developers of a Java application provide the DevOps team with a link to the GitLab code repository here: https://gitlab.com/nanuchi/java-maven-app.git. As a first step, the Java application project will be cloned to the local workstation.

1.	Using the following commands, open Command Prompt, change to the root of C:\ and clone the project locally. The project will be renamed as “complete-pipeline-tf-ec2.”

        cd ../..
        git clone https://gitlab.com/nanuchi/java-maven-app.git complete-pipeline-tf-ec2

    The output below validates the project is successfully cloned.

    ![1cl](img/prelim/clone/1.png)
 
2.	Minimize Microsoft Visual Studio Code and login to GitLab. In the right corner of the Projects screen, click the <b>New Project</b> button.  

    ![2cl](img/prelim/clone/2.png)

3.	On the Create new project scren, click Create blank project.
 
    ![3cl](img/prelim/clone/3.png)

4.	On the Create a blank project screen, specify the project name as complete-pipeline-tf-ec2. In the Project URL section, click the username dropdown and select your user (e.g. jlomba09). Ensure the Project Slug value matches the project name of complete-pipeline-tf-ec2. This project’s visibility level is set to Public but can be changed to Private. In the Project Configuration section, uncheck “Initialize repository with README.” Click the <b>Create project</b> button.

    ![4cl](img/prelim/clone/4.png) 

    Minimize GitLab.

5.	Restore the Command Prompt, change to the project folder, and remove the existing git folder by entering the following commands:

        cd complete-pipeline-tf-ec2
        rd /s /q .git

6.	Initialize the Git repository with the main branch. Then, add the GitLab project URL as the remote source.

        git init --initial-branch=main
        git remote add origin git@gitlab.com:username/complete-pipeline-tf-ec2.git

7.	Add all changes from the working directory into the staging area. Perform the initial commit.

        git add .
        git commit -m “Initial commit”

    ![5cl](img/prelim/clone/5.png) 

8.	Set the push behavior to map the local main branch to the remote main branch:

        git push –-set-upstream origin main
 
    ![6cl](img/prelim/clone/6.png) 

9.	Restore GitLab in the browser and refresh the screen. The initial project files will populate.

    ![7cl](img/prelim/clone/7.png) 

10.	Open File Explorer and confirm the project folder “complete-pipeline-tf-ec2” exists. Confirm the the project files exist inside.

    ![8cl](img/prelim/clone/8.png)
 
11.	Open Microsoft Visual Studio Code. At the top left, click on the Hamburger Menu (≡) > File > Open Folder.

    ![9cl](img/prelim/clone/9.png) 

12.	Browse to the project name at the root of C:\ and click the <b>Select Folder</b> button.

    ![10cl](img/prelim/clone/10.png)
 

13.	When prompted, click the <b>Yes, I trust the authors</b> button.
 
    ![11cl](img/prelim/clone/11.png)

14.	The folder hierarchy of the project will show in the left pane.

    ![12cl](img/prelim/clone/12.png) 

15.	Delete the script.groovy file. Click on the Jenkinsfile and clear the file contents. 

    ![13cl](img/prelim/clone/13.png)
 
    Minimize the IntelliJ IDEA code editor.

## B) Create Shared Library <a name="createSharedLib"></a>

1.	Open the IntelliJ IDEA code editor. On the Welcome screen, click the <b>New Project</b> button.

    ![1csl](img/prelim/create_shared_lib/1.png) 

2.	On the New Project screen, name the project “jenkins-shared-library,” specify the location as the root of C:\ and ensure the Groovy language is selected. Leave the default Build system as IntelliJ and leave the default JDK. Check the box “Add sample code” and click the <b>Create</b> button.

    ![2csl](img/prelim/create_shared_lib/2.png)  

3.	Right-click the root folder of the project > New > Directory.

    ![3csl](img/prelim/create_shared_lib/3.png)  

4.	Name the new directory as “vars” and press Enter.

    ![4csl](img/prelim/create_shared_lib/4.png) 
 
5.	The vars folder now appears in the root of the project folder. The vars folder will contain separate Groovy files containing functions; these functions will later be called/executed from both stages of the CI pipeline in Jenkinsfile.
 
    ![5csl](img/prelim/create_shared_lib/5.png) 

6.	Minimize the IntelliJ IDEA code editor. Open a web browser and login to GitLab. Click the <b>New Project</b> button.
 
    ![6csl](img/prelim/create_shared_lib/6.png) 

7.	On the Create new project screen, click Create blank project. 
 
    ![7csl](img/prelim/create_shared_lib/7.png) 

8.	On the Create blank project screen, name the project “jenkins-shared-library.” In the Project URL section, specify the username as the dropdown (e.g. jlomba09), and ensure the project slug matches the project name. Set the visibility level to Public and uncheck the box to initialize the repository with a README. Click the <b>Create project</b> button.
 
    ![8csl](img/prelim/create_shared_lib/8.png) 

9.	Open a Command Prompt and change to the C:\jenkins-shared-library directory.  Initialize the git repository with the following command:

        git init

    The output will show the repository is initialized.
 
    ![9csl](img/prelim/create_shared_lib/9.png) 

10.	Set the origin as the newly created jenkins-shared-library project in GitLab.

        git remote add origin git@gitlab.com:jlomba09/jenkins-shared-library.git

11.	Add changes and set an initial commit message.

        git add .
        git commit -m “Initial commit”

12.	Set the upstream push behavior to the master branch of the GitLab repository:
        
        git push -u origin master

13.	Refresh the jenkins-shared-library project in GitLab and confirm the project files exist:
 
    ![10csl](img/prelim/create_shared_lib/10.png) 

## C) Jenkins: Confirm/Add GitLab Credentials <a name="gitlabCreds"></a>
To verify the gitlab-credentials exist, login to the Jenkins management UI and navigate to Dashboard > Manage Jenkins > Credentials > System > Global credentials (unrestricted). If the GitLab credentials are present, no further configuration is required. 

If “gitlab-credentials” is missing from Jenkins, click the <b>+ Add Credentials</b> button:<br>- Specify the kind as “Username and password”<br>- Specify the GitLab user (e.g. jlomba09) as the username<br>- Enter the GitLab password<br>- Specify gitlab-credentials as the ID and description

Click the Save button.
 
![1glc](img/prelim/gitlab_creds/1.png) 

## D) Update Jenkinsfile <a name="updateJF1"></a>
For larger projects that span teams and multiple pipelines, a shared library can be defined globally in the Jenkins management UI. However, assume in this case the shared library is only applicable to one or two individual pipelines, and in turn, will be referenced directly within Jenkinsfile.

Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. At this point, the Jenkinsfile will be empty. Copy and paste the following code into Jenkinsfile (<ins>note</ins>: username will differ):

    #!/usr/bin/env groovy

    library identifier: 'jenkins-shared-library@master', retriever: modernSCM(
        [$class: 'GitSCMSource',
        remote: 'https://gitlab.com/jlomba09/jenkins-shared-library.git',
        credentialsId: 'gitlab-credentials'
        ]
    )

The shebang line tells the Microsoft Visual Studio Code editor that the file is written in Groovy script.

The library identifier specifies the master branch of the shared library. For the remote key, the value is specified as the GitLab URL to the newly created shared library. The remote attribute specifies the shared library URL, with the credentialsID attribute referencing the credentials stored in Jenkins.

## II. CI Stages: <a name="ciStages"></a>
Next, two Continuous Integration (CI) stages of the Jenkins pipeline will be written. The first stage will build a Maven artifact for the Java application. The second stage will build a Docker image, login to Docker, and push the Docker image to a private DockerHub repository. Prior to writing the CI stages in Jenkinsfile, this project includes writing the code for a shared library to make it usable in both CI stages of the pipeline. The shared library is reuseable logic that can be used for future pipelines and across teams, increasing efficiency of subsequent pipeline creation.
## A) Stage: Build App <a name="buildApp"></a>
This section is broken down into three parts. The first part addresses the Maven build tool prerequisites. The second part outlines creating the buildJar.groovy file in the shared library. This section concludes with writing the 'build app' stage in Jenkinsfile.

## i) Prerequisite: Maven <a name="mavenPrereq"></a>
A Maven command will be used in the buildJar.groovy file of the shared library to build a JAR file for the Java application. The buildJar.groovy file will then be called as a function in the 'build app' stage of the pipeline. For this to work, however, the Maven build tool must be present in Jenkins. 

This section confirms the Maven build tool exists in Jenkins and updates the Jenkinsfile to indicate the Maven build tool will be used in the pipeline.

## a) Jenkins: Confirm/Configure Build Tool <a name="mavenPlugin"></a>
The Maven build tool makes it possible for Jenkins to run Maven commands to build the JAR file for the Java application.

1.	Login to the Jenkins management UI and navigate to Dashboard > Manage Jenkins > Tools. Expand the Maven installations dropdown.

    ![1mp](img/ci_stages/buildapp/prereq/maven_plugin/1.png)
 
2.	Confirm the latest version of Maven is installed. If not, click the <b>Add Maven</b> button and mirror the same settings as the screenshot below. Click the <b>Save</b> button.

    ![2mp](img/ci_stages/buildapp/prereq/maven_plugin/2.png) 

## b) Update Jenkinsfile <a name="updateJF2"></a>
With the Maven build tool available in Jenkins, Maven can now be declared in the tools block inside Jenkinsfile. Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. Open Jenkinsfile, and directly under the shared library import statement, enter a couple carriage returns. Copy and paste the following code:

    pipeline {
      agent any
      tools {
        maven 'Maven'
    }


The pipeline block in Jenkinsfile is required and declares a pipeline. 

agent any tells Jenkins to run the entire pipeline on any agent that is available.

The tools section specifies that the Maven tool configured in section a) above will be used in the pipeline. The value ‘Maven’ is the same name provided for the Maven installation name.

## ii) Shared Library: Create buildJar.groovy <a name="buildJar"></a>
Inside the shared library, a buildJar.groovy file will contain the logic for building a JAR file for the Java application. As part of the shared library, the buildJar function associated with this file can be used in future projects.
1.	Restore the shared library project in the IntelliJ IDEA code editor. Right-click the vars folder > New > File.

    ![1slbj](img/ci_stages/buildapp/shared_lib_buildjar/1.png) 

2.	Name the file buildJar.groovy and press Enter. The Groovy file now appears under the vars folder.
 
    ![2slbj](img/ci_stages/buildapp/shared_lib_buildjar/2.png) 

3.	Copy and paste the following code into buildJar.groovy:

        #!/usr/bin/env groovy

        def call() {
          echo 'building the application...'
          sh 'mvn package'
        }


    The buildJar.groovy file begins with a shebang telling the code editor the file is written in Groovy script. A call definition code block is then created. Inside the call definition, the first line of code is an echo command displaying output to the user that the application is being built; second, a Maven command is issued to build the Java artifact as a JAR file.
4.  Push the changes of jenkins-shared-library to GitLab.

## iii) Update Jenkinsfile (buildJar) <a name="updateJF3"></a>
This section outlines writing the 'build app' stage in Jenkinsfile.

Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. Open Jenkinsfile, and directly user the tools block, create a stages block. Inside the stages block, create a 'build app' stage block. Under the ‘build app’ stage, create a steps block. Inside the steps block, create a script block. The below code illustrates these changes:

    stages {
        stage('build app') {
            steps {
               script {
                  buildJar()
               }
            }
        }
    }

Inside the script block, notice the buildJar function added above references the Groovy filename from the jenkins-shared-library. 

Save the Jenkinsfile, add and commit the changes, and push to the GitLab repository. 

    git add .
    git commit -m "Add build app stage to Jenkinsfile"
    git push

## B) Stage: Build Image <a name="buildImage"></a>
This section is comprised of three parts: (1) the shared library prerequisites, (2) updating the shared library, and (3) writing the 'build image' stage in Jenkinsfile. 
## i) Shared Library Prerequisites: <a name="sharedLibPrereq"></a>
The shared library prerequisites include confirming/configuring the private repo in DockerHub, confirming the Credentials Binding plugin exists in Jenkins, confirming/installing Docker inside the Jenkins container, creating DockerHub credentials in the Jenkins management UI, and creating the Dockerfile. These prerequisites will be discussed next.
## a) DockerHub: Confirm Private Repo <a name="dockerhub"></a>
In this project, DockerHub will be used as the private Docker image repository for the Java application. This section confirms the existance of the private Docker repository, and if missing, provides instructions for its creation.
1.	In a browser, navigate to the DockerHub website at https://hub.docker.com. Enter the username/email address associated with the DockerHub account. Click the <b>Sign in</b> button.
 
    ![1dh](img/ci_stages/buildimage/shared_lib_prereq/dockerhub/1.png)

2.	Enter the password associated with the Docker account. Click the <b>Continue</b> button.

    ![2dh](img/ci_stages/buildimage/shared_lib_prereq/dockerhub/2.png) 

3.	Confirm the <ins>private</ins> Docker image repository exists. A free tier account allows one private repository. As the private Docker image repository exists, the prerequisite is satisfied and the engineer can proceed to the next section.

    ![3dh](img/ci_stages/buildimage/shared_lib_prereq/dockerhub/3.png)

4.	If a private Docker image repository does not exist, click the <b>Create repository</b> button from the previous screenshot. On the Create repository screen, specify the repository name as “my-repo,” and under the Visibility section, select the Private radio button. Click the <b>Create</b> button.
 
    ![4dh](img/ci_stages/buildimage/shared_lib_prereq/dockerhub/4.png)

## b) Jenkins <a name="jpII"></a>
The Jenkins prerequisites pertaining to the shared library include confirming the Credentials Binding plugin exists in the Jenkins management UI, confirming and installing Docker inside the Linux host and inside the container where Jenkins resides, and creating DockerHub credentials to be stored inside Jenkins. 
## 1) Credentials Binding Plugin <a name="credBindings"></a>
The Credentials Binding plugin allows a Jenkins pipeline to leverage credentials stored inside Jenkins. For this project, the Credentials Binding plugin allows the withCredentials block to be used in the shared library's Docker Groovy class. Details about using the Credentials Binding plugin is found here: https://plugins.jenkins.io/credentials-binding/. Usage of the Credentials Binding plugin and the withCredentials block will be covered later in the Docker Class section of this project walkthrough.

Login to the Jenkins server UI and navigate to Dashboard > Manage Jenkins > Plugins. Click the Installed Plugins option in the left menu and search for “credentials.” If the Credentials Binding plugin shows as enabled, proceed to the next prerequisite. 

![1cbp](img/ci_stages/buildimage/shared_lib_prereq/jenkins/credentials_bindings_plugin/1.png) 

Otherwise, click the Available plugins menu option in the left menu, search for the Credentials Binding plugin, and enable the plugin.

## 2) Confirm/Install Docker <a name="installDocker"></a>
As the Docker Groovy class of the shared library will use Docker commands, Docker must be available inside both the Linux host server where the Jenkins container resides and inside the Jenkins container itself. 

Please refer to the following GitLab link for detailed steps on installing Docker on the Linux host server and making docker commands available inside the Jenkins container: https://gitlab.com/jlomba09/complete-pipeline-ecr-eks#c-confirminstall-docker

## 3) DockerHub Credentials <a name="dockerCreds"></a>
The Docker Class will leverage Docker commands to login to DockerHub and push a Docker image to the private DockerHub repository. For these commands to work, the DockerHub credentials must be made available inside Jenkins for authentication.

Login to the Jenkins management UI and navigate to Dashboard > Manage Jenkins > Credentials > System > Global credentials (unrestricted). Verify DockerHub credentials are present:

![1dc](img/ci_stages/buildimage/shared_lib_prereq/jenkins/docker_creds/1.png) 

If “docker-credentials” is missing from Jenkins, click the <b>+ Add Credentials button:</b><br>- Specify the Kind dropdown as “Username and password”<br>- Specify the DockerHub user (e.g. jlomba09) as the username<br>- Enter the DockerHub password<br>- Specify docker-credentials as the ID and description<br>- Click the <b>Save</b> button.

## c) Create Dockerfile <a name="dockerfile"></a>
This section outlines creating a Dockerfile to run the Java application as a container.

1. Restore the IntelliJ IDEA code editor and create a new file called Dockerfile.

2. Copy and paste the following code into the Dockerfile:

        FROM openjdk:8-jre-alpine
        EXPOSE 8080
        COPY ./target/java-maven-app-*.jar /usr/app/
        WORKDIR /usr/app
        CMD java -jar java-maven-app-*.jar

    The FROM statement specifies using a base image of Alpine Linux with Java 8 installed. Port 8080 is exposed, and the JAR file is copied from the Git repository to the /usr/app directory of the Alpine Linux container. The /usr/app path is the working directory, and finally, the java command runs the application. 
3. Save the Dockerfile.

## ii) Update Shared Library <a name="updateSharedLib"></a>
This section begins by outlining the creation of a Groovy class called Docker in the shared library. This section concludes with updating the Groovy files inside the vars folder.

## a) Create Groovy Class: Docker <a name="groovyClass"></a>

Within a shared library, the need for a Groovy class arises when the same command (e.g. docker) is used to perform different functions (e.g. docker build, docker login, docker push). As both the docker build and docker push commands require the image name as a parameter, creating a Groovy class in this case avoids code duplication so that both functions can reference the parameter in one location. The Docker class will be defined in the src package folder, which provides a centralized location for helper/utility code of the Docker functions.

1.	Restore the IntelliJ IDEA code editor. Right-click on src > New > Package. 

    ![1dgc](img/ci_stages/buildimage/update_shared_lib/groovy_class/1.png) 

2.	Name the package com.example and press Enter.
 
    ![2dgc](img/ci_stages/buildimage/update_shared_lib/groovy_class/2.png) 

3.	Right-click com.example > New > Groovy Class.

    ![3dgc](img/ci_stages/buildimage/update_shared_lib/groovy_class/3.png)  

4.	Name the Groovy Class as Docker.groovy and press Enter. As this is the name of a class, the d in Docker must be capitalized. 

    ![4dgc](img/ci_stages/buildimage/update_shared_lib/groovy_class/4.png)  

5.	At the Add File to Git dialog box, click the <b>Add</b> button.

    ![5dgc](img/ci_stages/buildimage/update_shared_lib/groovy_class/5.png)  

6.	Notice some starter code will populate in the Docker.groovy file.
 
    ![6dgc](img/ci_stages/buildimage/update_shared_lib/groovy_class/6.png) 

7.	At the top of the Docker.groovy file, add a shebang to tell the IntelliJ IDEA code editor the file is written in Groovy. This also helps with ensuring there are no extra parentheses and all parentheses are closed.

        #!/usr/bin/env groovy

8.	Leave the prepopulated package definition “package com.example” as the second line of code.

9.	At the end of the Docker class definition, add <b>implements Serializable</b> to save the execution state whenever the pipeline is paused and then resumed.

        class Docker implements Serializable {
        }

    The remaining code for the Docker.groovy file will be written inside the Docker class definition block.

10.	Inside the Docker class definition block, allow all aspects of the Jenkinsfile (e.g. shell commands, environment variables, credentials, plugins) to be accessible in the Docker.groovy class through the script variable by following these steps:<br>- Define a variable called script<br>- Pass script as a parameter to the Docker class<br>- Inside the Docker(script) block, create an instance of script by setting this.script = script. 

    This is summarized below:

        def script

        Docker(script) {
            this.script = script
            
        }

11.	Directly underneath the Docker(script) block, define a function/method called buildDockerImage, set the data type as String, and pass imageName as a parameter. 

        def buildDockerImage(String imageName) {

        }

12.	Inside the buildDockerImage function, display output to the user that the docker image is being built. Then, issue the <b>docker build</b> command specifying the Docker repository and tag as the $imageName variable. Recall the script prefix allows the docker command from Jenkins to be accessible in the Docker Groovy class.

        script.echo "building the docker image…"
        script.sh "docker build -t $imageName ."

13.	Under the buildDockerImage function, create a new function called dockerLogin. No parameters will be passed for this function:

        def dockerLogin() {

        }

14.	Inside the dockerLogin function, copy and paste the following code:

        script.withCredentials([script.usernamePassword(credentialsId: 'docker-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        }

- The script prefix allows the withCredentials block from Jenkinsfile to be available in the Docker Groovy class.
- The withCredentials block leverages the Credentials Binding plugin in Jenkins and allows access to the docker-credentials stored in Jenkins: https://plugins.jenkins.io/credentials-binding/. 

15.	Inside the script.withCredentials block, copy and paste the following code:

        script.sh "echo '${script.PASS}' | docker login -u '${script.USER}' --password-stdin"

- The script prefix is needed to make the echo command, username, and password from Jenkins available in the Docker Groovy class.
- An echo command pipes the referenced Docker password stored in Jenkins as standard input in the docker login command. 
- The USER variable in the docker login command references the username stored in docker-credentials in Jenkins.
- The USER and PASS variables are enclosed with single quotes and curvy brackets so the $ is not confused to be a character.
- The docker login command does not require a registry URL as DockerHub is understood as the default.

16.	Under the dockerLogin function, create a new function called dockerPush. Specify the data type as String and pass imageName as a parameter:

        def dockerPush(String imageName) {

        }

17.	Inside the dockerPush function, copy and paste the following code to push the Docker image to the DockerHub private repository. For a final time, the script prefix is used to make the docker command from Jenkins available in the Docker Groovy class.

        script.sh "docker push $imageName"

18.	The Docker Groovy class is complete. Please reference src/com/example/Docker.groovy for the final code. As Main.groovy is not being used, delete it from the src folder. Add, commit, and push changes to GitLab.

## b) Update vars folder <a name="vars"></a>
Now that the Docker Groovy class is available from the shared library, filenames matching the Groovy functions will be created inside the vars folder. The functions can then be called upon inside Jenkinsfile.

## 1) buildImage.groovy <a name="buildImageGroovy"></a>
1.	Restore the shared library project in the IntelliJ IDEA code editor. Right-click the vars folder > New > File
 
    ![1uvbi](img/ci_stages/buildimage/update_shared_lib/update_vars/buildImage/1.png)

2.	Name the file buildImage.groovy and press Enter.

    ![2uvbi](img/ci_stages/buildimage/update_shared_lib/update_vars/buildImage/2.png) 

3.	Begin the buildImage.groovy file with a shebang that tells the IntelliJ IDEA code editor the file will be written in Groovy script. Import the Docker class.

        #!/usr/bin/env groovy

        import com.example.Docker

4.	A call definition block is then created, passing imageName as a parameter with data type String.

        def call(String imageName) {

        }

5.	Inside the call definition, return the result of a new instance’s buildDockerImage function from the Docker class, where: <br>- The ‘this’ keyword passes in the current content, thereby allowing access to commands, environment variables, plugins, tools, etc. from the Jenkinsfile. <br>- The buildDockerImage passes imageName as a parameter

        return new Docker(this).buildDockerImage(imageName)

6. Add changes, commit, and push to GitLab.
## 2) dockerLogin.groovy <a name="dockerLogin"></a>

1.	Right-click the vars folder > New > File. Name the file dockerLogin.groovy.

    ![1uvdl](img/ci_stages/buildimage/update_shared_lib/update_vars/dockerLogin/1.png) 

2.	Begin the dockerLogin.groovy file with a shebang that tells the IntelliJ IDEA code editor the file will be written in Groovy script. Import the Docker class.

        #!/usr/bin/env groovy

        import com.example.Docker

3.	A call definition block is then created, passing no parameters.

        def call() {

        }
4.	Inside the call definition, return the result of a new instance’s dockerLogin function from the Docker class, where: <br>- The ‘this’ keyword passes in the current context, thereby allowing access to commands, environment variables, plugins, tools, etc. from the Jenkinsfile. 

        return new Docker(this).dockerLogin()

5.	Add changes, commit, and push to GitLab.

## 3) dockerPush.groovy <a name="dockerPush"></a>

1.	Right-click the vars folder > New > File. Name the file dockerPush.groovy.

    ![1uvdp](img/ci_stages/buildimage/update_shared_lib/update_vars/dockerPush/1.png) 

2.	Copy and paste the entire contents of buildImage.groovy. Change the function name to dockerPush.

        #!/usr/bin/env groovy

        import com.example.Docker

        def call(String imageName) {
          return new Docker(this).dockerPush(imageName)

        }

3.	Add changes, commit, and push to GitLab.

The shared library is complete and can found here: https://gitlab.com/jlomba09/jenkins-shared-library
## iii) Update Jenkinsfile <a name="updateJF4"></a>
With the shared library complete, the shared library can now be referenced in the second stage of the Jenkins pipeline.
1.	Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code and open Jenkinsfile. Under the 'build app' stage, create a new stage called 'build image.' Nest the steps and script blocks as shown below:

        stage(‘build image’) {
            steps {
                  script {
                  }
            }
        }

2.	As both the buildImage and dockerPush functions from the shared library expect the repository name and tag as a required element in their respective docker commands, under the tools block of Jenkinsfile, create an environment block. Assign the IMAGE_NAME variable with the value of the repository and denote the tag after the colon:

        environment {
            IMAGE_NAME = 'jlomba09/my-repo:jma-1.0'
        }

3.	In the scripts block of the 'build image' stage, call the buildImage, dockerLogin, and dockerPush functions by their corresponding Groovy filenames from the vars folders in the shared library. For the buildImage and dockerPush functions, pass the IMAGE_NAME as a parameter appending the env prefix. Recall that dockerLogin does not have any parameters.

        buildImage(env.IMAGE_NAME)
        dockerLogin()
        dockerPush(env.IMAGE_NAME)

4.	Save the Jenkinsfile, add all changes, commit, and push to the GitLab repository. 

        git add .
        git commit -m "Add build app stage to Jenkinsfile"
        git push

## III. CD Stages: <a name="cdStages"></a>
Next, two Continuous Delivery/Deployment (CD) stages of the Jenkins pipeline will be written. The first stage will automatically provision an EC2 instance using Terraform. The second stage will use a Linux bash script to run Docker Compose and deploy a Docker image to the EC2 instance.

##     A) Stage: Provision EC2 Server Using Terraform <a name="provision"></a>
This section is comprised of two major parts: (1) addressing AWS, Jenkins, Terraform, and bash prerequisites and (2) writing the 'provision server' stage in Jenkinsfile.
         
## i) Prerequisites: <a name="prereqIII"></a>
Prior to writing the 'provision server' stage in Jenkinsfile, several prerequisites must be met. First, the SSH key pair must be created in AWS. Then, in Jenkins, the SSH Plugin must be available and a skeleton pipeline will be created. The SSH key pair credentials will be added to Jenkins management UI, and then terraform will be installed inside the Jenkins container. Next, Terraform code will be written to provision a VPC and the EC2 instance and set variable values. Last, a bash script will be created to execute during EC2 initialization. 
               
## a) AWS: Create SSH Key Pair  <a name="awsSSH"></a>
Although a SSH key pair in AWS can be created with Terraform, for this project, the key pair will be created manually and specified later in the Terraform configuration file.
1. Login to the AWS Management Console. In the search bar, type in EC2 and navigate to the EC2 management page.
 
    ![1aws_ssh](img/cd_stages/provision_ec2_tf/prereq/aws_ssh_key_pair/1.png)

2. On the left navigation pane, scroll down to the Network & Security section. Click on Key Pairs.

    ![2aws_ssh](img/cd_stages/provision_ec2_tf/prereq/aws_ssh_key_pair/2.png) 

3. In the Key pairs pane on the right, click the <b>Create key pair</b> button.

    ![3aws_ssh](img/cd_stages/provision_ec2_tf/prereq/aws_ssh_key_pair/3.png) 
 
4.	On the Create key pair screen, specify the name as myapp-key-pair. Leave the key pair type as the default RSA option. Select the private key file format as .pem. Click the <b>Create key pair</b> button.

    ![4aws_ssh](img/cd_stages/provision_ec2_tf/prereq/aws_ssh_key_pair/4.png) 

5.	Confirm the key pair appears on the screen.
 
    ![5aws_ssh](img/cd_stages/provision_ec2_tf/prereq/aws_ssh_key_pair/5.png) 

6.	Open File Explorer and confirm the key pair is available in the Downloads folder.
 
    ![6aws_ssh](img/cd_stages/provision_ec2_tf/prereq/aws_ssh_key_pair/6.png) 

## b) Jenkins: <a name="jpIII"></a>
Prior to writing the 'provision server' stage, four prerequisites in Jenkins must be satisfied. First, a pipeline skeleton will be created. Then, credentials for the SSH key pair and AWS will be stored in Jenkins. Last, Terraform will be installed inside the Jenkins container.
## 1) Create Pipeline Skeleton <a name="skeleton"></a>
1.	Login to the Jenkins management UI. On the Dashboard screen, click +New Item in the left navigation menu. 

    ![1js](img/cd_stages/provision_ec2_tf/prereq/jenkins/skeleton/1.png) 

2.	Enter the pipeline job as “complete-pipeline-tf-ec2” and select Multibranch Pipeline. Click <b>OK</b>.
 
    ![2js](img/cd_stages/provision_ec2_tf/prereq/jenkins/skeleton/2.png) 

3.	On the Configuration > General screen, click the <b>Save</b> button at the bottom. Additional configuration of the pipeline will be addressed later.
 
    ![3js](img/cd_stages/provision_ec2_tf/prereq/jenkins/skeleton/3.png) 

## 2) Credentials for SSH Key Pair <a name="credsSSH"></a>
1.	Clicking the <b>Save</b> button from the last section will load the Dashboard > complete-pipeline-tf-ec2 screen. In the left navigation pane, click <b>Credentials</b>.

    ![1jssh](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_ssh_key_pair/1.png)  

2.	On the Credentials screen, scroll down to the <b>Stores scoped to complete-pipeline-tf-ec2</b> section. Click on (global).

    ![2jssh](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_ssh_key_pair/2.png)  
 
3.	On the Global credentials (unrestricted) screen, click the <b>+ Add Credentials</b> button.

    ![3jssh](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_ssh_key_pair/3.png)  
 
4.	On the New credentials screen, select SSH Username with private key from the Kind dropdown. Enter the ID and Description as server-ssh-key. The default user for logging into EC2 instances is ec2-user, so specify that as the username. In the Private Key section, select the radio button “Enter directly.” In the key subsection, click the <b>Add</b> button.

    ![4jssh](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_ssh_key_pair/4.png)  
 
5.	The Key subsection will then change to a text box where the myapp-key-pair.pem can be pasted. Minimize the Jenkins management UI. Open Windows PowerShell and type the following command to display the contents of the .pem file.

        type ~\Downloads\myapp-key-pair.pem

    Starting from -----BEGIN RSA PRIVATE KEY----- and including -----END RSA PRIVATE KEY-----, copy the entire private key to the clipboard.

    ![5jssh](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_ssh_key_pair/5.png)   

6.	Restore the Jenkins management UI and paste the contents of the clipboard inside the Key textbox. Click the <b>Create</b> button at the bottom of the New credentials screen.

    ![6jssh](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_ssh_key_pair/6.png)  

7.	Confirm the server-ssh-key credentials now shows on the screen.

    ![7jssh](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_ssh_key_pair/7.png)   

This server-ssh-key will later be referenced in the final 'deploy' stage of the pipeline. In the 'deploy' stage, Jenkins will SSH into the EC2 server and execute ssh and scp commands to deploy the Java application. 

## 3) Credentials for AWS <a name="credsAWS"></a>
1. In the Jenkins management UI, navigate to Dashboard > complete-pipeline-tf-ec2. On the complete-pipeline-tf-ec2 screen, click Credentials on the left menu.

    ![1jaws](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_aws/1.png)  

2. Under the “Stores scoped to complete-pipeline-tf-ec2” section, locate the complete-pipeline-tf-ec2 store and click on the (global) domain.
  
    ![2jaws](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_aws/2.png) 

3. Click the <b>+ Add Credentials</b> button at the top right of the screen. The “New credentials” screen will now display.

    ![3jaws](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_aws/3.png) 
 
4. <ins>Note</ins>: While the installation of the AWS CLI is outside the scope of this project walkthrough, installation instructions are found here: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html. The AWS CLI is used to run the <b>aws configure</b> command to add the AWS account's access key to ~/.aws/credentials on the local machine. If necessary, new access keys can be created from the AWS IAM management page. 

    Minimize the Jenkins management UI window and open PowerShell. Enter the following command in PowerShell to view the AWS credentials for programmatic access:

        type ~/.aws/credentials
 
    Minimize the PowerShell window.
5. Restore the Jenkins management UI window. On the New Credentials page, specify “Secret text” from the Kind dropdown menu and specify the ID as jenkins_aws_access_key_id. Restore the PowerShell window and copy and paste the value for the aws_access_key_id into the Secret textbox. Click the <b>Create</b> button.

    ![4jaws](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_aws/4.png) 
 
6. Repeat Steps 3-5, but this time, for the secret access key.
 
7. Confirm both Secret text credentials appear on the screen.

    ![5jaws](img/cd_stages/provision_ec2_tf/prereq/jenkins/creds_aws/5.png) 
  
AWS credentials are now successfully added to Jenkins.
## 4) Install Terraform <a name="installtf"></a>
For Terraform commands to be issued in the pipeline, Terraform must be installed inside the Jenkins container.
1.	SSH into the Linux server that hosts the Jenkins container. Run the following command to reveal the container ID for the Jenkins server. Copy the container ID to the clipboard.

        docker ps

2.	Enter the container as root, pasting the container ID from the clipboard:

        docker exec -it -u0 <container-id> bash

3.	Download the HashiCorp key

        curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -

    The curl and wget commands can generally be used interchangeably to download items (e.g. files, archive folders) from the Internet. Per the curl man page (https://curl.se/docs/manpage.html), the -f option is a short form of –fail and is synonymous with fail fast. It indicates not to display any output when a server error occurs. The -s option specifies silent mode and the -S option is a short form of --show-error. When -s and -S are used together, the curl commands displays an error in the event the command fails. The -L option is a short form of --location and specifies the URL address of the HashiCorp key. 

    The curl command is then piped to the apt-key command, which “…is used to manage the list of keys used…to authenticate packages” (https://manpages.ubuntu.com/manpages/focal/man8/apt-key.8.html). The apt-key add – command adds the HashiCorp key to the list of managed keyrings. 

4.	Use apt-get to install the software-properties-common package and gain access to the apt-add-repository command.

        apt-get install software-properties-common

5.	Use the apt-add-repository command to add the official HashiCorp repository into /etc/apt/sources.list or /etc/apt/source.list.d (https://manpages.ubuntu.com/manpages/trusty/man1/add-apt-repository.1.html): 

        apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

6.	Update the list of packages and install Terraform

        apt-get update && apt-get install terraform

7.	Confirm Terraform is installed:

        terraform -v

    ![1it](img/cd_stages/provision_ec2_tf/prereq/jenkins/install_tf/1.png)

## c) Terraform Configuration Files <a name="tfconfig"></a>
With Terraform installed on the Jenkins server, the Terraform configuration files can now be written.
1.	Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. Click the folder icon to create a folder called terraform. 

    ![1tfc](img/cd_stages/provision_ec2_tf/prereq/tf_config/1.png) 

2.	Right-click the terraform folder > New File… and name it main.tf. Minimize the code editor.

    ![2tfc](img/cd_stages/provision_ec2_tf/prereq/tf_config/2.png) 

3.	In a browser, navigate to Terraform’s official AWS provider page (https://registry.terraform.io/providers/hashicorp/aws/latest).  Click the <b>Use Provider</b> button and copy the code to the clipboard.

    ![3tfc](img/cd_stages/provision_ec2_tf/prereq/tf_config/3.png)  

4.	Restore Microsoft Visual Studio Code and begin the main.tf configuration file by pasting the contents from the clipboard. 

        terraform {
          required_providers {
            aws = {
              source = "hashicorp/aws"
              version = "5.20.1"
            }
          }
        }

        provider "aws" {
          # Configuration options
        }

5.	Delete the configuration options comment from the “aws” provider block and add a region argument. Parameterize the value for the region argument, assigning its value as var.region. Declare the region input variable under the “aws” provider block. This is summarized below: 

        provider “aws” {
           region = var.region
        }

        variable region {}
	
    Minimize the Microsoft Visual Studio Code editor.

## 1)	main.tf: VPC Resources & Inputs <a name="maintfVPC"></a>
This section creates a Terraform configuration file for automatically provisioning a VPC.
1.	In a browser, navigate to the official Terraform documentation on how to create a VPC resource (https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc). Copy the basic usage example to the clipboard. 

2.	Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. Under the provider “aws” block, paste the contents of the basic usage example from the clipboard.

        resource "aws_vpc" "main" {
          cidr_block = "10.0.0.0/16"
        }

3.	Change the name of the VPC resource from “main” to “myapp-vpc” as shown below:

        resource "aws_vpc" "myapp-vpc"

4.	Under the cidr_block argument of the “aws_vpc” resource block, create a tags block. Inside the tags block, add the Name attribute. The Name attribute denotes the VPC will be deployed in the Development environment:

        tags = {
            Name = "dev-vpc"
        }

5.	To make this Terraform configuration file reusable for other environments aside from Development and to prevent hardcoding of IP addresses in main.tf with security in mind, every argument’s value in the Terraform configuration file moving forward will be parameterized. Thus far, the cidr_block value and value of the tag argument’s Name attribute can be parameterized. Under the provider “aws” block, first declare the input variables.

        variable vpc_cidr_block {}
        variable env_prefix {}

    In the aws_vpc block:<br>- Update the value of the cidr_block argument to var.vpc_cidr_block<br>- In the tags argument, update the value of the Name argument to “${var.env-prefix}-vpc”

6.	From the previous step, confirm the code underneath the provider “aws” block now looks like this:

        variable vpc_cidr_block {}
        variable env_prefix {}

        resource "aws_vpc" "myapp-vpc" {
            cidr_block = var.vpc_cidr_block
            tags = {
                Name = "${var.env-prefix}-vpc"
            }
        }

7.	In a browser, navigate to the official Terraform documentation on how to create an AWS subnet resource (https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet).  Copy the basic usage example to the clipboard. 

8.	Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. Enter a couple carriage returns under the “aws_vpc” block and paste the contents from the clipboard.

        resource "aws_subnet" "main" {
          vpc_id     = aws_vpc.main.id
          cidr_block = "10.0.1.0/24"

          tags = {
            Name = "Main"
          }
        }

9.	Change the name of the AWS subnet resource from “main” to “myapp-subnet-1” as shown below:

        resource "aws_subnet" "myapp-subnet-1"

10.	Notice from the official documentation that the vpc_id is a required attribute for the “aws_subnet” resource. However, the VPC ID will not be known until after the VPC resource is created. Terraform allows the extraction of the unknown vpc_id value from the VPC resource by using the following syntax:<br><br>
<i>resource_type.resource_name.attribute<i><br> or<br> 
aws_vpc.myapp-vpc.id

    As such, update the value of the vpc_id argument by replacing “main” with “myapp-vpc” as shown below:

    <ins>Before</ins>:

    vpc_id = aws_vpc.main.id

    <ins>After</ins>:

    vpc_id = aws_vpc.myapp-vpc.id

11.	Under the cidr_block argument of the “aws_subnet” resource block:<br>- Add availability_zone as an argument. This project assumes us-east-2a.<br>
-Copy and paste the tag block from Step 6 but change the suffix value from “vpc” to “subnet-1”

        availability_zone = “us-east-2a”
        tags = {
            Name = "${var.env-prefix}-subnet-1"
        }

12.	Parameterize the values of cidr_block and availability_zone. Under the existing input variables, declare the new input variables.

        variable subnet_cidr_block {}
        variable avail_zone {}

    In the aws_vpc block:
    - Update the value of the cidr_block argument to var.subnet_cidr_block
    - Update the value of the availability zone argument to var.avail_zone
    - The value of the Name argument in the tags block is already parameterized from previous steps.

13.	From the previous step, confirm the code underneath the provider VPC resource block now looks like this:

        variable subnet_cidr_block {}
        variable avail_zone {}

        resource "aws_subnet" "myapp-subnet-1" {
            vpc_id     = aws_vpc.myapp-vpc.id
            cidr_block = var.subnet_cidr_block
            availability_zone = var.avail_zone
            tags = {
                Name = "${var.env-prefix}-subnet-1"
            }
        }

14.	In a browser, navigate to the official Terraform documentation on how to create an AWS Internet Gateway resource (https://registry.terraform.io/providers/hashicorp/aws/3.6.0/docs/resources/internet_gateway). Copy the basic usage example to the clipboard.

15.	Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. Enter a couple carriage returns under the “aws_subnet” block and paste the contents from the clipboard.

        resource "aws_internet_gateway" "gw" {
            vpc_id = aws_vpc.main.id

            tags = {
                Name = "main"
            }
        }

16.	Change the name of the AWS Internet Gateway resource from “gw” to “myapp-igw” as shown below:

        resource "aws_internet_gateway" "myapp-igw"

17.	Copy and paste the vpc_id argument from the “aws subnet” resource. Copy and paste the tag block from Step 6 but change the suffix value from “vpc” to “igw.” There are no new input variables to add to the top of the main.tf file. The final “aws_internet_gateway” resource configuration looks like this:

        resource "aws_internet_gateway" "myapp-igw" {
            vpc_id     = aws_vpc.myapp-vpc.id

            tags = {
                Name = "${var.env-prefix}-igw"
            }
        }

18.	In a browser, navigate to the official Terraform documentation on how to create an AWS default route table (https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_route_table). Copy the example usage to the clipboard.

19.	Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. Enter a couple carriage returns under the “aws_internet_gateway” block and paste the contents from the clipboard.

        resource "aws_default_route_table" "example" {
            default_route_table_id = aws_vpc.example.default_route_table_id

            route {
                cidr_block = "10.0.1.0/24"
                gateway_id = aws_internet_gateway.example.id
            }

            route {
                ipv6_cidr_block        = "::/0"
                egress_only_gateway_id = aws_egress_only_internet_gateway.example.id
            }

            tags = {
                Name = "example"
            }
        }

    Like in Step 10, notice from the official documentation that the default_route_table_id is a required attribute for the “aws_default_route_table” resource. However, the Default Route Table ID will not be known until after the VPC resource is created. Terraform allows the extraction of the unknown default_route_table_id value from the VPC resource with the following syntax:<br><br><i>resource_type.resource_name.attribute</i><br> or<br>aws_vpc.myapp-vpc.default_route_table_id

20.	Make the following changes to the “aws_default_route_table” block:<br>- Change the name of the AWS Default Route table to “main-rtb”<br>- For the default_route_table_id argument, change the value of the VPC name to myapp-vpc<br>- In the first route block, change the cidr_block to “0.0.0.0/0”<br>- For the gateway_id argument in the route block, change the name of the Internet Gateway from “example” to myapp-igw<br>- Delete the route block for IPv6<br>- Copy and paste the tag block from Step 6 but change the suffix value from “vpc” to “-main-rtb”

    The below code summarizes these changes:

        resource "aws_default_route_table" "main-rtb" {
            default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id

            route {
                cidr_block = "0.0.0.0/0"
                gateway_id = aws_internet_gateway.myapp-igw.id
            }

            tags = {
                Name = "${var.env-prefix}-main-rtb"
            }
        }

21.	In a browser, navigate to the official Terraform documentation on how to create an AWS default security group (https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_security_group).  Copy and paste the example usage to the clipboard.

22.	Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. Enter a couple carriage returns under the “aws_default_route_table” block and paste the contents from the clipboard.

        resource "aws_vpc" "mainvpc" {
            cidr_block = "10.1.0.0/16"
        }

        resource "aws_default_security_group" "default" {
            vpc_id = aws_vpc.mainvpc.id

            ingress {
                protocol  = -1
                self      = true
                from_port = 0
                to_port   = 0
            }

            egress {
                from_port   = 0
                to_port     = 0
                protocol    = "-1"
                cidr_blocks = ["0.0.0.0/0"]
            }
        }

23.	As a VPC is already defined, delete the redundant “aws_vpc” block. Make the following changes to the “aws_default_security_group” block:<br>- Change the name of the AWS default security group from “default” to “default-sg”<br>- In the vpc_id argument, change the name the VPC from “mainvpc” to “myapp-vpc”<br>- In the ingress block, allow SSH access to both the engineer’s local machine and the Jenkins server by specifying “tcp” as the protocol and the from_port and to_port as 22. Parameterize the value of the cidr_blocks argument with [var.my_ip, var.jenkins_ip]. Declare the my_ip and jenkins_ip input variables with the others at the top of main.tf<br>- Copy the ingress block, enter a couple carriage returns, and paste a duplicate copy of the ingress block. Change the second ingress block to allow access to port 8080 from anywhere by specifying “tcp” as the protocol, the from_port and to_port as 8080, and the array inside the cidr_blocks argument as 0.0.0.0/0<br>- In the egress block, add the prefix_list_ids argument with an empty array []<br>- Copy and paste the tag block from Step 6 but change the suffix value from “vpc” to “-default-sg”

    The below code summarizes these changes:

        variable my_ip {}
        variable jenkins_ip {}

        resource "aws_default_security_group" "default-sg" {
            vpc_id     = aws_vpc.myapp-vpc.id

            ingress {
                protocol  = "tcp"
                from_port = 22
                to_port   = 22
                cidr_blocks = [var.my_ip, var.jenkins_ip]
            }

            ingress {
                protocol  = "tcp"
                from_port = 8080
                to_port   = 8080
                cidr_blocks = ["0.0.0.0/0"]
            }

            egress {
                from_port   = 0
                to_port     = 0
                protocol    = "-1"
                cidr_blocks = ["0.0.0.0/0"]
                prefix_list_ids = []
            }

            tags = {
                Name = "${var.env-prefix}-default-sg"
            }
        }

The VPC portion of the Terraform configuration file is complete.

## 2)	main.tf: EC2 Resources, Data, & Inputs <a name="maintfEC2"></a>
Next, the main.tf Terraform configuration file will be completed to include resources, data, and inputs required to provision an EC2 instance. A single output for displaying the EC2 IP address in the Jenkins pipeline is also covered.
1.	In a browser, navigate to the official Terraform documentation on how to create a data source for AWS AMI (https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami). Copy the example usage to the clipboard. Minimize the web browser.

2.	Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. Enter a couple carriage returns under the “aws_default_security_group” block and paste the contents from the clipboard.

        data "aws_ami" "example" {
            executable_users = ["self"]
            most_recent      = true
            name_regex       = "^myami-\\d{3}"
            owners           = ["self"]

            filter {
                name   = "name"
                values = ["myami-*"]
            }

            filter {
                name   = "root-device-type"
                values = ["ebs"]
            }

            filter {
                name   = "virtualization-type"
                values = ["hvm"]
            }
        }

    Minimize the Microsoft Visual Studio Code editor.

3.	The values argument under the first filter block will need to be changed from “myami-*” to the latest version of Amazon Linux 2023. Obtaining the name of the AMI image can easily be done inside the AWS Management Console. Steps 3 through 7 help to uncover the AMI name. 

    Restore the web browser and login to the AWS Management Console. Type ec2 into the search bar and navigate to the EC2 management page.

    ![1me](img/cd_stages/provision_ec2_tf/prereq/tf_config/main_ec2/1.png) 

4.	In the left navigation pane, scroll down to the Images section. Click on AMI Catalog.

    ![2me](img/cd_stages/provision_ec2_tf/prereq/tf_config/main_ec2/2.png) 

5.	On the AMI Catalog screen, search for amazon linux to narrow down the results. Select the Amazon Linux 2023 AMI, confirm the 64-bit (x86) radio button is selected, and click the <b>Launch Instance with AMI</b> button. 

    <ins>Note:</ins> The instance will not actually be launched, but rather, this is a fast way to unveil the AMI ID. The ami- shown on the screen is not the value that Terraform expects! 

    ![3me](img/cd_stages/provision_ec2_tf/prereq/tf_config/main_ec2/3.png)

6.	On the Launch an instance screen, scroll to the Application and OS Images (Amazon Machine Image) section. On the AMI from catalog tab, the circled value below is what Terraform expects. Copy this value to the clipboard, cancel out of the wizard, and log out of the AWS Management Console.

    ![4me](img/cd_stages/provision_ec2_tf/prereq/tf_config/main_ec2/4.png) 

7.	Restore the Microsoft Visual Studio Code editor. In the “name” filter block under the “aws_ami” data source, replace “myami-*” by pasting the contents from the clipboard. As the image name will change upon subsequent releases, shorten the value to the following:

        values = [“al2023-ami-*-x86_64”]

8.	Make the following remaining changes to the “aws_ami” block:<br>- Change the name of the AWS AMI data source to “latest-amazon-linux-image”<br>- Delete the lines for executable_users and name_regex<br>- Change the value of the owners argument to “amazon”<br>- Delete the filter block for “root-device-type”

    The below code summarizes these changes:

        data "aws_ami" "latest-amazon-linux-image" {
            most_recent      = true
            owners           = ["amazon"]
            filter {
                name   = "name"
                values = ["al2023-ami-*-x86_64"]
            }
            filter {
                name   = "virtualization-type"
                values = ["hvm"]
            }
        }

9.	In a browser, navigate to the official Terraform documentation on how to create an AWS EC2 instance resource: (https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance). From the example usage, copy only the resource “aws_instance” block to the clipboard. 

10.	Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. Enter a couple carriage returns under the “aws_ami” block and paste the contents from the clipboard.

        resource "aws_instance" "web" {
            ami           = data.aws_ami.ubuntu.id
            instance_type = "t3.micro"

            tags = {
                Name = "HelloWorld"
            }
        }

11.	Make the following changes to the “aws_instance” block:<br>- Change the name of the aws_instance from “web” to “myapp-server”<br>- For the ami argument, change the AMI name to “latest-amazon-linux-image”<br>- Parameterize the instance_type value as var.instance_type<br>- Declare the instance_type input with the other variables at the top of main.tf<br>- Copy and paste the tag block from previous resources but change the suffix value to “-server”

    With these changes, the “aws_instance” block now looks like this:

        variable instance_type {}

        resource "aws_instance" "myapp-server" {
            ami           = data.aws_ami.latest-amazon-linux-image.id
            instance_type = var.instance_type 

            tags = {
                Name = "${var.env-prefix}-server"
            }
        }

12.	Add the following additional arguments to the aws_instance block:

        subnet_id = aws_subnet.myapp-subnet-1.id
        vpc_security_group_ids = [aws_default_security_group.default-sg.id]
        availability_zone = var.avail_zone

        associate_public_ip_address = true 
        key_name = "myapp-key-pair"

        user_data = file("entry-script.sh")

    Where:<br>
        - The subnet_id is extracted from the named subnet resource of myapp-subnet-1.<br>
        - The vpc_security_group_ids is an array containing the security group ID of default-sg.<br>
        - The availability_zone is set by the previously defined avail_zone input variable.<br>
        - The associate_public_ip_address is set to true so that the server can be accessed externally.<br>
        - The key_name is set to the name of the SSH key pair created in AWS in section III.A.i.a of this project walkthrough.<br>
        - The user_data argument specifies a file that will be created in section d) below.

13. In a browser, browse back to the official Terraform documentation on the aws_instance resource: (https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance). Notice at the bottom, public_ip is an available attribute. For Terraform to display the output of the public IP address in the Jenkins pipeline, copy the following into the Microsoft Visual Studio Code editor.

        output "ec2_public_ip" {
            value = aws_instance.myapp-server.public_ip
        }

    The public IP is extracted from the named myapp-server EC2 instance and will later be displayed as output when the Jenkinsfile stage is written.

The main.tf Terraform configuration file is complete.
## 3)	variables.tf <a name="variables"></a>
To set variable values for local Terraform execution, the terraform.tfvars file is employed. The terraform.tfvars file allows any engineer who has a local copy of the complete-pipeline-tf-ec2 project to configure their own values, though, and thus, the file is included in .gitignore. Since terraform.tfvars will not be checked into the GitLab repository, a separate file called variables.tf is necessary to set values for the Jenkins pipeline.
1.	Restore the Microsoft Visual Studio Code editor. Right-click the terraform folder > New > File and name the file as variables.tf.

    ![1v](img/cd_stages/provision_ec2_tf/prereq/tf_config/variables/1.png)
 
2.	Cut and paste the declared variable inputs from main.tf into the variables.tf file. Thus far, the variables.tf file looks like this:

        variable region {}
        variable vpc_cidr_block {}
        variable env_prefix {}
        variable subnet_cidr_block {}
        variable avail_zone {}
        variable my_ip {}
        variable jenkins_ip {}
        variable instance_type {}

3.	Include a default argument inside every variable block. Set the values as shown below (<ins>note</ins>: your IP and Jenkins server IP will differ):

        variable region {
            default = "us-east-2"
        }
        variable vpc_cidr_block {
            default = "10.0.0.0/16"
        }
        variable env_prefix {
            default = "dev"
        }
        variable subnet_cidr_block {
            default = "10.0.10.0/24"
        }
        variable avail_zone {
            default = "us-east-2a"
        }
        variable my_ip {
            default = "x.x.x.x/32"
        }
        variable jenkins_ip {
            default = "x.x.x.x/32"
        }
        variable instance_type {
            default = "t2.micro"
        }

The variables.tf configuration file is complete. Despite default values being set, any of these values can be overridden in Jenkinsfile using the special TF_VAR environment variable. 

## d) Bash Script: entry-script.sh <a name="bashEntry"></a>
Recall from the main.tf Terraform configuration file that the aws_instance resource uses the user_data argument to pass a bash script upon EC2 initialization. This bash script will be created next.
1.	In Microsoft Visual Studio Code, create a new file under the terraform folder called entry-script.sh.

    ![1pb](img/cd_stages/provision_ec2_tf/prereq/bash/1.png) 

2.	Start the entry-script.sh file with a shebang that tells the code editor this is a bash script.

        #!/bin/bash

3.	Next, a command is written to update the yum package repository list and install docker. The sudo command is required here as the command is not being issued by the root user. The -y flag makes it so that manual user intervention to press Y is not needed when prompted to continue the installation. The && allows multiple commands to be strung together on the same line.

        sudo yum update -y && sudo yum install -y docker

4.	The next two commands start the Docker service and add the ec2-user to the docker group.

        sudo systemctl start docker
        sudo usermod -aG docker ec2-user

5.	A couple carriage returns are entered and a comment is written to indicate the next section will be dedicated to installing the docker-compose command line tool.

        # install docker-compose

6.	The official Docker documentation is referenced for the remainder of the commands (https://docs.docker.com/compose/install/standalone/). 

    Use curl to download the package from the official Docker repository. The -o option saves the contents of the URL to the /usr/local/bin/docker-compose location.


        sudo curl -SL https://github.com/docker/compose/releases/download/v2.20.3/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose

7.	Add executable permission to the docker-compose file.

        sudo chmod +x /usr/local/bin/docker-compose

8.	In case the installation fails, add a symbolic link to the /usr/bin location.

        sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

With all prerequisites met, the 'provision server' stage of the pipeline can now be written.

## ii) Update Jenkinsfile <a name="updateJF5"></a>

1. Inside Microsoft Visual Studio Code, open Jenkinsfile. Directly under the 'build image' stage, create a new stage called 'provision server.' As done previously, create a steps block and inside the steps block, create a script block.

        stage('provision server') {
            steps {
                script {
                }
            }
        }

2. Between the 'provision server' stage and steps block, create an environment block at the same indentation level as the steps block. Recall one of the prerequisites to writing the provision server stage of the Jenkinsfile is creating AWS credentials inside the Jenkins management UI (Section III.A.i.b.3). These credentials will be called now, along with a special Terraform variable to override the env_prefix variable. Recall the default environment is “dev,” but specifying TF_VAR_env_prefix overrides the value to “test.”

        environment {
            AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
            AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
            TF_VAR_env_prefix = 'test'
        }

3.  Inside the script block, change to the terraform directory by creating a dir(‘terraform’) block. The remainder of the code for the 'provision server' stage will be written in this dir block. 

        script {
            dir (‘terraform’) {
            }
        } 

4.	Inside the dir(‘terraform’) block, write a shell command to initialize the terraform providers and apply the terraform configuration without user intervention via the –auto-approve flag.

        sh "terraform init"
        sh "terraform apply –auto-approve"

5.	Recall from Section III.A.i.c.2, Step 13 that an output named “ec2_public_ip” is created in the main.tf Terraform configuration file. To display the public IP address of the EC2 server as output when the Jenkins pipeline stage runs, copy and paste the following underneath the <b>terraform apply</b> command.

        EC2_PUBLIC_IP = sh(
            script: “terraform output ec2_public_ip”,
            returnStdout: true
        ).trim()

    The trim function removes any whitespace the output may produce. 

6.	The final 'provision server' stage looks like this:

        stage('provision server') {
            environment {
                AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
                AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
                TF_VAR_env_prefix = 'test'
            }
            steps {
                script {
                    dir('terraform') {
                        sh "terraform init"
                        sh "terraform apply --auto-approve"
                        EC2_PUBLIC_IP = sh(
                            script: "terraform output ec2_public_ip",
                            returnStdout: true
                        ).trim()
                    }
                }
            }
        }

7. Save the Jenkinsfile, add all changes, commit, and push to the GitLab repository. 

        git add .
        git commit -m "Add provision server stage to Jenkinsfile"
        git push

## B) Stage: Deploy new Java app version on EC2 with Docker Compose <a name="deploy"></a>

The final stage of the Jenkins pipeline will use a combination of bash scripting and docker compose to deploy a new application version to the Terraform provisioned EC2 instance.

## i) Prerequisites: <a name="prereqDeploy"></a>

Before the 'deploy' stage can be written in Jenkinsfile, a few prerequisites must be met. First, the SSH Agent plugin in Jenkins must be present. Second, a second bash script must be written to login to Docker and execute the docker-compose command. Last, a YAML file for Docker Compose must be written. These prerequisites are discussed next.

## a) SSH Agent Plugin <a name="sshagent"></a>

1.	Login to the Jenkins management UI. Navigate to Dashboard > Manage Jenkins > Plugins.

2.	Confirm whether the SSH Agent Plugin is installed by clicking Installed plugins from the left navigation menu and typing in ssh agent plugin. Since the plugin is installed, no further action is required. If the plugin is not installed, click Available plugins in the left navigation menu and install the SSH Agent Plugin.
 
    ![1dps](img/cd_stages/deploy/prereq/ssh_agent_plugin/1.png)

The SSH Agent Plugin will be used when writing the 'deploy' stage in Jenkinsfile; its usage will be denoted as sshagent inside the steps block. More information can be found here: https://plugins.jenkins.io/ssh-agent/  

## b) Bash Script: Docker Commands <a name="bashDocker"></a>

1.	Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. At the root of the project folder, create a new file called server-cmds.sh

    ![1db](img/cd_stages/deploy/prereq/bash/1.png) 

2.	Start the server-cmds.sh file with a shebang that tells the code editor this is a bash script.

        #!/bin/env bash

3.	Export 3 user inputs to be passed as parameters when the bash script is run in Jenkinsfile.

        export IMAGE=$1
        export DOCKER_USER=$2
        export DOCKER_PWD=$3

4.	Login to Docker using the DOCKER_USER and DOCKER_PWD parameters defined in the previous step. Pipe the Docker password variable so it can be passed as standard input. The -u option specifies the DOCKER_USER as the user.

        echo $DOCKER_PWD | docker login -u $DOCKER_USER --password-stdin

5.	Run the docker-compose command, specifying the docker-compose.yaml file. Run the containers in detached mode.

        docker-compose -f docker-compose.yaml up --detach

6.	Display “success” as user output on the screen.

        echo "success"

## c) Docker Compose <a name="dockercompose2"></a>

1.	Restore the complete-pipeline-tf-ec2 project in Microsoft Visual Studio Code. At the root of the project folder, create a new file called docker-compose.yaml.

    ![1ddc](img/cd_stages/deploy/prereq/docker_compose/1.png) 

2.	Begin the docker-compose.yaml file by specifying the file format version as 3.8 (https://docs.docker.com/compose/compose-file/compose-file-v3/)

        version: '3.8'

3.	Reference the following file for general Docker Compose syntax (https://docs.docker.com/compose/features-uses/). Create a services attribute under the version specification.

        services:

4.	Under services, specify the first container name as java-maven-app. Inside the java-maven app container specification, add an attribute for image and for ports. For the image attribute, use the image variable ${IMAGE} that is passed as the first user input from the bash script. Specify the container to run on port 8080.

        java-maven-app:
            image: ${IMAGE}
            ports:
              - 8080:8080

5.	Under services, create a new container specification for postgres. Create 3 attributes: image, ports, and environment. For the image attribute, specify the official Docker image ‘postgres:13’ (https://hub.docker.com/_/postgres). For the ports attribute, use port 5432 and set an environment variable for the PostgreSQL password.

        postgres:
            image: postgres:13
            ports:
              - 5432:5432
            environment:
              - POSTGRES_PASSWORD=my-pwd

    The docker-compose.yaml file is now complete. Now that all prerequisites are satisfied, the final 'deploy' stage of the pipeline can be written.

## ii) Update Jenkinsfile <a name="updateJF6"></a>

1. Inside Microsoft Visual Studio Code, open Jenkinsfile. Directly under the 'provision server' stage, create a new stage called 'deploy.' As done previously, create a steps block and inside the steps block, create a script block.

        stage('deploy) {
            steps {
               script {
               }
            }
        }

2. Inside the script block, display output to the user that the EC2 server is waiting to initialize. Call the built-in sleep function to wait 90 seconds to account for the delay between the time the EC2 is in the running state and the time it takes to finish the initialization process.

        echo "waiting for EC2 server to initialize"
        sleep(time: 90, unit: "SECONDS")

3. Still inside the script block, display output to the user that the docker image is being deployed to EC2. Also display output of the EC2_PUBLIC_IP variable created in the previous 'provision server' stage.

        echo 'deploying docker image to EC2...'
        echo "${EC2_PUBLIC_IP}"

4. Above the steps block, create an environment block. Create a variable called DOCKER_CREDS which will use docker-credentials created in section II.B.i.b.3 of this project walkthrough. 

        environment {
            DOCKER_CREDS = credentials('docker-credentials')
        }

5. Back in the script block, write a line to execute the server-cmds.sh bash script created in Section III.B.i.c of this project walkthrough. Recall this bash script expects 3 user inputs that are defined in the context of the bash script as export statements, where:<br>- Image is set to the first user input<br>- User is set to the second user input<br>- The password is set to the third user input.<br> 

    For the context of Jenkinsfile, set the first parameter passed as {$IMAGE_NAME}, which is defined at the top of the Jenkinsfile in the (global) environment block. Jenkinsfile allows the extraction of the DOCKER_CREDS stored in Jenkins separately for the username ${DOCKER_CREDS_USR} and password ${DOCKER_CREDS_PSW}. Set this bash script execution command to a variable called shellCmd.

        def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME} ${DOCKER_CREDS_USR} ${DOCKER_CREDS_PSW}"

6. As the to-be-written scp and ssh commands expect the user@IP syntax for the destination, create another variable called ec2Instance. Set the ec2Instance variable to use the user@IP synax with ec2-user as the username and EC2_PUBLIC_IP (the Terraform output defined in the previous 'provision server' stage) as the IP.

        def ec2Instance = “ec2-user@${EC2_PUBLIC_IP}”

7. Recall the SSH Agent plugin in Jenkins is a prerequisite before writing the 'deploy' stage in Jenkinsfile. Use the SSH Agent plugin by creating a sshagent block inside the script block. Specify ‘server-ssh-key’ as the credential name.

        sshagent(['server-ssh-key']) {
        }

8. Inside the sshagent block, the scp and ssh commands alluded to in Step 6 will now be written. 

    First, use secure copy (scp) commands to copy the server-cmds.sh and docker-compose.yaml files from the GitLab repository to the ec2-user’s home directory on the EC2 server. Set the StrictHostKeyChecking value to the value of no to bypass the host key check (yes/no prompt) that displays when initially logging into a server with SSH for the first time. 

        sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"

9. Finally, inside the sshagent block, issue an ssh command to run the bash script on the EC2 instance. Once again, set the StrictHostKeyChecking value to no.

        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"

The Jenkinsfile is now complete.

## IV. Pipeline Execution, Troubleshooting, & Validation <a name="pipeline"></a>

1.	Login to the Jenkins management UI. Click on the complete-pipeline-tf-ec2 job.

    ![1pipe](img/pipeline/1.png) 

2.	On the complete-pipeline-tf-ec2 screen, click Configure in the left navigation menu.
 
    ![2pipe](img/pipeline/2.png) 

3.	On the General screen, locate the Branch Sources section. Click the Add source dropdown and select Git.

    ![3pipe](img/pipeline/3.png) 

4.	In the Git section, copy and paste the project GitLab URL into the Project Repository textbox. From the credentials dropdown, select the credentials created in section I.C of this project walkthrough.

    ![4pipe](img/pipeline/4.png)  

5.	In the Behaviors section, select the “Filter by name (with regular expression)” in the dropdown. Enter the main branch as the regular expression.

    ![5pipe](img/pipeline/5.png)  

6.	Click the <b>Save</b> button at the bottom. The pipeline should start building. Navigate to the main branch of the pipeline to view the status.

    ![6pipe](img/pipeline/6.png)  

7.	<ins>Troubleshooting #1</ins>: The first issue occurs the first 16 builds of the pipeline job.

    ![7pipe](img/pipeline/7.png)  

    The pipeline fails at the 'provision server' stage. From the console output, an error displays at the bottom regarding a Null Pointer Exception. 

    ![8pipe](img/pipeline/8.png)  

    <ins>Resolution #1</ins>: This error is caused by the dir block in Jenkinsfile not specifying a folder to change into. After updating the Jenkinsfile with dir(‘terraform’), this error is resolved.

    <ins>Troubleshooting #2</ins>: The second error occurs from builds 17 through 19.

    ![9pipe](img/pipeline/9.png) 
 

    The pipeline again fails at the 'provision server' stage. From the console output, the issue is with env_prefix variable.

    ![10pipe](img/pipeline/10.png) 
 
    <ins>Resolution #2</ins>: Changing every instance of env-prefix to env_prefix in main.tf fixed this issue.

8.	With the two fixes made above, the pipeline is rerun successfully.

    ![11pipe](img/pipeline/11.png)  

9.	<ins>Validation</ins>: Login to the AWS Management Console and confirm the EC2 instance exists:

    ![12pipe](img/pipeline/12.png)  

    SSH into the EC2 server. From the console output of the last job, recall the IP address is displayed as an output:

    ![13pipe](img/pipeline/13.png)  

    Open the Command Prompt. SSH into the EC2 server as ec2-user, specifying the .pem file with the -i option. The .pem file should still reside in the Downloads folder.

        cd Downloads
        ssh -i myapp-key-pair.pem ec2-user@3.14.130.196

    Run the <b>ls</b> command to confirm the docker-compose.yaml and bash script are copied to the ec2-user’s home directory.

    ![14pipe](img/pipeline/14.png)  

    Run the <b>docker ps</b> command to confirm the Java application and PostgreSQL containers are running:

    ![15pipe](img/pipeline/15.png)  

## V. Project Teardown <a name="teardown"></a>
1. The Terraform state is local to Jenkins and not the engineer’s laptop. If the <b>terraform plan</b> command is executed locally, Terraform will not be aware the infrastructure is already provisioned in AWS. To have one state available locally and in Jenkins, an AWS S3 bucket will need to be created. In the terraform block, the following code will need to be added to main.tf:

        terraform {
            backend "s3" {
                bucket = "myapp-bucket"
                key = "myapp/state.tfstate"
                region = "us-east-2"
            }
        }

    However, for the sake of this project, the <b>terraform destroy</b> command will be run from the build replay in Jenkins.

2.	In the Jenkins management UI, click the last successful build number.

    ![1tear](img/teardown/1.png)  

3.	On the build screen, click Replay in the left menu.

    ![2tear](img/teardown/2.png)   

4.	In the Main Script box, scroll to the 'provision server' stage. Delete the existing terraform commands and write the following command:

        terraform destroy –auto-approve

    ![3tear](img/teardown/3.png)  
 
5.	In the Main Script box, scroll to the 'deploy' stage. Delete all commands except the echo command to wait for the EC2 server.

    ![4tear](img/teardown/4.png)  
 
6.	Click the <b>Run</b> button at the bottom.

    ![5tear](img/teardown/5.png)   

7.	Viewing the console output of this new build shows that Terraform is destroying the infrastructure.

    ![6tear](img/teardown/6.png)  

8.	The pipeline completes successfully.

    ![7tear](img/teardown/7.png)  
 
9.	Login to the AWS Management Console and confirm the EC2 instance is terminated.

    ![8tear](img/teardown/8.png)  
 

10.	In the AWS Management Console, navigate to the VPC Dashboard. Confirm all defaults have been restored.

    ![9tear](img/teardown/9.png)  

11.	Optionally, rerun the pipeline again to ensure the result is successful through all stages. To not incur unnecessary costs in AWS, repeat the steps outlined in this section to delete all resources when finished.
