variable region {
    default = "us-east-2"
}
variable vpc_cidr_block {
    default = "10.0.0.0/16"
}
variable env_prefix {
    default = "dev"
}
variable subnet_cidr_block {
    default = "10.0.10.0/24"
}
variable avail_zone {
    default = "us-east-2a"
}
variable my_ip {
    default = "71.192.193.95/32"
}
variable jenkins_ip {
    default = "157.230.178.235/32"
}
variable instance_type {
    default = "t2.micro"
}
